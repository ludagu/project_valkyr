import react from "react";
import "./card.css";

const StatArea = (props) => {
  return (
    <div className="stats-area">
      <div className="stat-div">
        <h3 className="stat-name">STR:</h3>
        <h4 className="stat-value">{props.monster.STR}</h4>
      </div>
      <div className="stat-div">
        <h3 className="stat-name">DEX:</h3>
        <h4 className="stat-value">{props.monster.DEX}</h4>
      </div>
      <div className="stat-div">
        <h3 className="stat-name">CON:</h3>
        <h4 className="stat-value">{props.monster.CON}</h4>
      </div>
      <div className="stat-div">
        <h3 className="stat-name">INT:</h3>
        <h4 className="stat-value">{props.monster.INT}</h4>
      </div>
      <div className="stat-div">
        <h3 className="stat-name">WIS:</h3>
        <h4 className="stat-value">{props.monster.WIS}</h4>
      </div>
      <div className="stat-div">
        <h3 className="stat-name">CHA:</h3>
        <h4 className="stat-value">{props.monster.CHA}</h4>
      </div>
    </div>
  );
};

function handleCLick(name) {
  console.log(name);
}

export const Card = (props) => {
  return (
    <div
      className="card_container"
      onClick={() => handleCLick(props.monster.name)}
    >
      <center>
        <div className="monster-name">
          <h1 className="monster-name-h1">{props.monster.name}</h1>
        </div>
        <div className="monster-pic-stats">
          <div className="stats-area">
            <div className="stat-div">
              <h3 className="stat-name">HP:</h3>
              <center>
                <h4 className="stat-value">{props.monster.Hit_Points}</h4>
              </center>
            </div>
            <div className="stat-div">
              <h3 className="stat-name">AC:</h3>
              <h4 className="stat-value">{props.monster.Armor_Class}</h4>
            </div>
          </div>
          <img className="card_img" alt="monster" src={props.monster.img_url} />

          <div className="stats-area">
            <div className="stat-div">
              <h3 className="stat-name">STR:</h3>
              <center>
                <h4 className="stat-value">{props.monster.STR}</h4>
              </center>
            </div>
            <div className="stat-div">
              <h3 className="stat-name">DEX:</h3>
              <h4 className="stat-value">{props.monster.DEX}</h4>
            </div>
            <div className="stat-div">
              <h3 className="stat-name">CON:</h3>
              <h4 className="stat-value">{props.monster.CON}</h4>
            </div>
            <div className="stat-div">
              <h3 className="stat-name">INT:</h3>
              <h4 className="stat-value">{props.monster.INT}</h4>
            </div>
            <div className="stat-div">
              <h3 className="stat-name">WIS:</h3>
              <h4 className="stat-value">{props.monster.WIS}</h4>
            </div>
            <div className="stat-div">
              <h3 className="stat-name">CHA:</h3>
              <h4 className="stat-value">{props.monster.CHA}</h4>
            </div>
          </div>
        </div>
      </center>

      <div className="monster-info">
        <p>{props.monster.info}</p>
      </div>
    </div>
  );
};
