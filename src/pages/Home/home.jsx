import { Carousel, Card, CardDeck } from "react-bootstrap";
import PicturesGrid from "../monster_manual";
import Col from "react-bootstrap/Col";

const HomeCard = () => {
  return (
    <CardDeck>
      <Card xs="auto">
        <Card.Img
          variant="top"
          src="https://i.ytimg.com/vi/4M0884xGP_Q/maxresdefault.jpg"
        />
        <Card.Body>
          <Card.Title>SOURCEBOOKS</Card.Title>
          <Card.Text>
            This is a wider card with supporting text below as a natural lead-in
            to additional content. This content is a little bit longer.
          </Card.Text>
        </Card.Body>
        <Card.Footer>
          <small className="text-muted">Last updated 3 mins ago</small>
        </Card.Footer>
      </Card>
      <Card>
        <Card.Img
          variant="top"
          src="https://i.ytimg.com/vi/bbLr2wfO9KI/maxresdefault.jpg"
        />
        <Card.Body>
          <Card.Title>ADVENTURES</Card.Title>
          <Card.Text>
            This card has supporting text below as a natural lead-in to
            additional content.{" "}
          </Card.Text>
        </Card.Body>
        <Card.Footer>
          <small className="text-muted">Last updated 3 mins ago</small>
        </Card.Footer>
      </Card>
    </CardDeck>
  );
};

const HomeCarousel = () => {
  return (
    <Carousel>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="https://media.dnd.wizards.com/styles/second_hubpage_banner/public/images/head-banner/NEW-TO-DnD_What-is-DnD_Subsection_Hero_140718.jpg"
          alt="First slide"
        />
        <Carousel.Caption>
          <h3>First slide label</h3>
          <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
        </Carousel.Caption>
      </Carousel.Item>

      <Carousel.Item>
        <img
          className="d-block w-100"
          src="https://media.dnd.wizards.com/styles/story_banner/public/images/head-banner/hero_dmgscreen_0.jpg"
          alt="Second slide"
        />

        <Carousel.Caption>
          <h3>Second slide label</h3>
          <p>
            Praesent commodo cursus magna, vel scelerisque nisl consectetur.
          </p>
        </Carousel.Caption>
      </Carousel.Item>

      <Carousel.Item>
        <img
          className="d-block w-100"
          src="https://media.dnd.wizards.com/styles/story_banner/public/images/head-banner/HERO_RiseTiamat_1.jpg"
          alt="Third slide"
        />

        <Carousel.Caption>
          <h3>Third slide label</h3>
          <p>
            Praesent commodo cursus magna, vel scelerisque nisl consectetur.
          </p>
        </Carousel.Caption>
      </Carousel.Item>
    </Carousel>
  );
};
//---------------------------------Home----------------------------------
const Home = () => {
  return (
    <div>
      <HomeCarousel />
      <div className="sm-pad">
        <HomeCard />
      </div>
      <h1>Boddy</h1>
      <PicturesGrid />
    </div>
  );
};

export default Home;
