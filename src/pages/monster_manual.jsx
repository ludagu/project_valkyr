import React, { Component } from "react";
import { CardList } from "../global/components/card-list/card-list";
import { SearchBox } from "../global/components/search_box/search_box";

class PicturesGrid extends Component {
  constructor() {
    super();

    this.state = {
      monsters: [],
      searchField: "",
      modalVisible: false,
    };
  }

  componentDidMount() {
    fetch(
      "https://raw.githubusercontent.com/NE0RI0/jsons_collection/main/dndMonsters.json"
    )
      .then((response) => response.json())
      .then((users) => this.setState({ monsters: users }));
  }

  handleChange = (e) => {
    this.setState({ searchField: e.target.value });
  };

  setModal(visible) {
    this.setState({ modalVisible: visible });
  }

  render() {
    const { monsters, searchField } = this.state;
    const filteredMonsters = monsters.filter((monster) =>
      monster.name.toLowerCase().includes(searchField.toLocaleLowerCase())
    );
    return (
      <div className="App">
        <div className="emotions-picture">
          <h1>Monster Manual</h1>
        </div>

        <SearchBox
          placeholder="Search Category"
          handleChange={this.handleChange}
        />
        <CardList monsters={filteredMonsters}>
          modalVisible = {() => this.setModal(true)}
        </CardList>
      </div>
    );
  }
}

export default PicturesGrid;
