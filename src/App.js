import "./App.css";
import "bootstrap/dist/css/bootstrap.css";
import { BrowserRouter as Router, Route } from "react-router-dom";
import { AuthProvider } from "./Log/Auth";
import PrivateRoute from "./Log/PrivateRoute";
import NaviBar from "./Components/NavBar/navbar";
import LogIn from "./pages/LogIn/login";
import SignUp from "./pages/SignUp/signup";
import Home from "./pages/Home/home";
import Classes from "./pages/Classes/classes";
import Footer from "./Components/Footer/footer";
import "./global/global.css";

function App() {
  return (
    <AuthProvider>
      <NaviBar />
      <Router>
        <div className="app-div">
          <Route exact path="/" component={Home} />
          <Route exact path="/login" component={LogIn} />
          <Route exact path="/signup" component={SignUp} />
        </div>
      </Router>
      <Footer />
    </AuthProvider>
  );
}

export default App;
