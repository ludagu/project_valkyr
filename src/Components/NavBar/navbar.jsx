import "bootstrap/dist/css/bootstrap.css";
import {
  Navbar,
  Nav,
  NavDropdown,
  Button,
  FormControl,
  Form,
} from "react-bootstrap";
import "./navbar.styles.css";

const NaviBar = () => {
  return (
    <div className="navBar-Div">
      <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
        <Navbar.Brand href="#home">D&D Valhalla</Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse className="justify-content-end">
          <Nav className="mr-auto">
            <Nav.Link href="#features">Features</Nav.Link>
            <Nav.Link href="#pricing">Pricing</Nav.Link>
            <NavDropdown title="Dropdown" id="collasible-nav-dropdown">
              <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
              <NavDropdown.Item href="#action/3.2">
                Another action
              </NavDropdown.Item>
              <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
              <NavDropdown.Divider />
              <NavDropdown.Item href="#action/3.4">
                Separated link
              </NavDropdown.Item>
            </NavDropdown>
          </Nav>
          <Navbar.Text>
            Signed in as: <a href="#login">Mark Otto</a>
          </Navbar.Text>
        </Navbar.Collapse>
      </Navbar>
      <hr className="horizontal-ruler" />
      <div className="nav-links-div">
        <a className="nav-links">Races</a>
        <a className="nav-links">Classes</a>
        <a className="nav-links">Spells</a>
        <a className="nav-links">Monster</a>
        <a className="nav-links">Feats</a>
        <a className="nav-links">Backgrouds</a>
        <a className="nav-links">Equipment</a>
        <a className="nav-links">Magic-Items</a>
        <a className="nav-links">Vehicles</a>
        <a className="nav-links">Backgrouds</a>
        <a className="nav-links">Game-Rules</a>
      </div>
    </div>
  );
};

export default NaviBar;
